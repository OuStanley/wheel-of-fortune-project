from utils import lowercase, key_of_max
import string
from collections import OrderedDict    # Variant of dict that you might want to learn about

#
# WordSet class
#
class WordSet:
    """Set of unique words, all in lower case and of positive length.

    >>> WordSet("one two, Two. tHree").words()
    ['one', 'three', 'two']
    >>> WordSet(["one","two","Two", ""]).words()
    ['one', 'two']
    >>> 'two' in WordSet(["one","two","Two"])
    True
    """
    def __init__(self, text):
        """Form a WordSet from a string of words or collection of words.
        """
        # BEGIN Question 2
        self.text = text
        self.duplicated_word_set = []
        self.c = []
        self.word_set = []        
        
        # END Question 2

    def words(self):
        """Return sorted list of words in WordSet.

        >>> WordSet("Hi. Hey you. How, the heck, are you?").words()
        ['are', 'heck', 'hey', 'hi', 'how', 'the', 'you']
        """
        # BEGIN Question 2
        
        #Removes the punctuation
        if type(self.text) == str:
            for i in lowercase(self.text):
                if i not in string.punctuation:
                    self.duplicated_word_set.append(i)
            self.duplicated_word_set = ''.join(self.duplicated_word_set).split()
        
        #Removes the duplicates and sorts them by alphabetical order
            for i in self.duplicated_word_set:
                if i not in self.word_set:
                    self.word_set.append(i)
        
            return sorted(self.word_set)
    
        elif type(self.text) == list:
            for i in self.text:
                lowercase(i)
                self.duplicated_word_set.append(lowercase(i))
            for i in self.duplicated_word_set:
                if i not in string.punctuation:
                    self.c.append(i)
            for i in self.c:
                if i not in self.word_set:
                    self.word_set.append(i)


            return self.word_set
    
        # END Question 2

    def __contains__(self, word):
        # BEGIN Question 2
        return word in self.text
        # END Question 2



#
# Dictionary class
#
class Dictionary(WordSet):
    """Construct a dictionary from all the words in a text file.
    Subclass of WordSet with a file based initializer.

    >>> from wordset import Dictionary
    >>> Dictionary('assets/lincoln.txt').words()[55]
    'government'
    """
    def __init__(self, filename):
        with open(filename) as fp:
            text = fp.read()
            WordSet.__init__(self, text)

#
# WordMunch class
#

class WordMunch(WordSet):
    """Perform analytics on a set of unique words.

    Subclass of WordSet that provides analytics on the words.

    >>> w = WordMunch("one two, Two. tHree")
    >>> w.words()
    ['one', 'three', 'two']
    >>> w.frequency()['o']
    2
    >>> key_of_max(w.frequency())
    'e'
    """
    
    def filter(self, ffun):
        """Filter set to include only those that satisfy the filter function predicate."""
        # BEGIN

        filtered_list = []
        self = WordMunch(self.words())
        list_of_words = self.words()
        for i in list_of_words:
            if ffun(i) == True:
                filtered_list.append(i)
        return filtered_list
    
                
            
        # END

    def frequency(self):
        """Return an ordered dictionary of the frequency of each letter in the word set."""
        # BEGIN

        #create an empty dictionary of all letters and 0 counts
        list_of_zeroes = []
        list_of_alphabetical_letters = list(string.ascii_lowercase)
        for i in range(len(string.ascii_lowercase)):
            list_of_zeroes.append(0)
            dictionary = dict(zip(list_of_alphabetical_letters,list_of_zeroes))

        #update the dictionary with the letter count
        self = WordMunch(self.text)
        list_of_words = self.words()
        list_of_letters = []
    
        for word in list_of_words:
            for letter in word:
                list_of_letters.append(letter)

        for i in list_of_letters:
            for alphabetical_letter in list_of_letters:
                letter_count = list_of_letters.count(alphabetical_letter)
                dictionary[alphabetical_letter] = letter_count

        return dictionary
        
                
         
                        
            
            
            
            
                        
                
                
        # END
