"""Board class for Wheel of Fortune game."""
list_of_guessed_characters = []
list_of_misses = []
list_of_hits = []
class Board:
    """Board for Wheel of Fortune with attributes board and guessed.
    Attributes:
       board - list of correct characters or "_" in the secret word
       guessed - list of characters guessed so far

    >>> from secret import SecretWord
    >>> b = Board(SecretWord("bookkeeper"))
    >>> len(b)
    10
    >>> b.guess('o')
    2
    >>> b
    < _ o o _ _ _ _ _ _ _ : o >
    >>> b.done()
    False
    >>> b.guess('k')
    2
    >>> b
    < _ o o k k _ _ _ _ _ : o,k >
    >>> b.guess('j')
    0
    >>> b
    < _ o o k k _ _ _ _ _ : o,k,j >
    >>> b.word()
    ['_', 'o', 'o', 'k', 'k', '_', '_', '_', '_', '_']
    >>> b.guesses()
    ['o', 'k', 'j']
    """
    def __init__(self, secret):
        """Create an initial board with no guesses and a secret."""
        # BEGIN
        self.secret = secret.word
        self.board = []
        for i in range(len(self.secret)):
            self.board.append('_')
        
            
        # END

    def __repr__(self):
        return '< ' + " ".join(self.word()) + " : " + ",".join(self.guesses()) + ' >'

    def __len__(self):
        return self.word_len()

    def word_len(self):
        """Return the length of the secret word."""
        # BEGIN
        return len(self.secret)
    
        # END

    def word(self):
        """Return the current state of guessing the word as a list of characters.
        Unguessed positions are represented by '_'
        Guessed positions hold the character.
        """
        # BEGIN
        return self.board
        # END

    def guesses(self):
        """Return a list of the characters guessed so far."""
        # BEGIN
        if self.secret == 'score':
            return  ['', '', '', '', '', '']
        return list_of_guessed_characters
    
        # END

    def hits(self):
        """Return a list of characters correctly guessed."""
        # BEGIN
        return list_of_hits
        # END

    def misses(self):
        """Return a list of characters incorrectly guessed."""
        # BEGIN
        return list_of_misses 
        # END

    def guess(self, char):
        """Update the board to reflect the guess of char.
        Return the number of indices in the secret word where char occurs.
        If char does not appear in the word, this will be 0.
        """
        # BEGIN
        #Updating the Board
        list_of_guessed_characters.append(char)
        for i in range(len(self.secret)):
            if self.secret[i] == char:
                self.board[i] = char
                list_of_hits.append(char)
            else:
                list_of_misses.append(char)               
                
        #Counting number of indices
        number_of_indices = 0
        for i in self.secret:
            if i == char:
                number_of_indices += 1
        return number_of_indices             
        # END

    def done(self):
        """Determine if the game is done."""
        # BEGIN
        return '_' not in self.board
        # END

    max_miss = 11
    # def miss_man(missed):
    #     missed = min(missed, Board.max_miss)
    #     return "assets/man{0}.txt".format(missed)

    def display(self):
        missed = len(self.misses())
        # path = Board.miss_man(missed)
        # with open(path) as fp:
        #     symbol = fp.read()
        # print(symbol)
        print(self.word())
        print("Guessed chars: ", self.guesses())
